# SPDX-License-Identifier: ISC

REL=73
SITEXX=site${REL}.tgz
O=output

.PHONY: tgz
tgz: ${O}/${SITEXX}

.PHONY: clean
clean:
	rm -rf ${O}

${O}/${SITEXX}:
	mkdir ${O}
	tar -czvf ${@} -C src/ .
